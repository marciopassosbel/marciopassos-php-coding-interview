<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\models\ClientModel;
use Src\models\DiscountModel;

class BookingTest extends TestCase {

	private $booking;
	private $createdBookingIds = [];
	private $clientModel;
	private $discountModel;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
		$this->clientModel = new ClientModel();
		$this->discountModel = new DiscountModel();
		$this->createdBookingIds = [];
	}

	public function tearDown(): void
	{
		// delete test data
		foreach ($this->createdBookingIds as $bookingId) {
			$this->booking->deleteBooking($bookingId);
		}

		parent::tearDown();
	}

	/** @test */
	public function getBookings() {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		if (!empty($results)) {
			// Get the first booking
			$booking = reset($results);
			$this->assertArrayHasKey('id', $booking);
			$this->assertArrayHasKey('clientid', $booking);
			$this->assertArrayHasKey('price', $booking);
			$this->assertArrayHasKey('checkindate', $booking);
			$this->assertArrayHasKey('checkoutdate', $booking);
		}
	}

	/** @test */
	public function createBooking() {
		$newBooking = [
			'clientid' => 1, // Ensure this is a valid client ID.
			'price' => 1000,
			'checkindate' => "2021-08-04 15:00:00",
			'checkoutdate' => "2021-08-11 15:00:00"
		];
		$bookings = $this->booking->getBookings();

		$createdBooking = $this->booking->createBooking($newBooking);
		if ($createdBooking) {
			$this->createdBookingIds[] = $createdBooking['id']; // Track the new booking ID
		}

		// get client with $createdBooking['clientid']
		$client = $this->clientModel->getClientById($createdBooking['clientid']);
		// check discounts
		$isClientEligibleForDiscount = $this->discountModel->checkIfClientIsEligibleForDiscount($client);

		if ($isClientEligibleForDiscount) {
			$this->assertNotEquals($newBooking['price'], $createdBooking['price'], "Discount was not applied and client is eligible");
		} else {
			$this->assertEquals($newBooking['price'], $createdBooking['price'], "Discount was applied but not client not eligible");
		}

		// Validate the booking was created with the correct details.
		$this->assertIsArray($createdBooking);
		$this->assertEquals($newBooking['clientid'], $createdBooking['clientid']);
		$this->assertEquals($newBooking['checkindate'], $createdBooking['checkindate']);
		$this->assertEquals($newBooking['checkoutdate'], $createdBooking['checkoutdate']);
	}
}
