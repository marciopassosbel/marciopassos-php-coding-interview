<?php

namespace Src\controllers;

use Src\models\BookingModel;

class Booking {

	private function getBookingModel(): BookingModel {
		return new BookingModel();
	}

	public function getBookings(): array
	{
		return $this->getBookingModel()->getBookings();
	}

	public function createBooking($client): array
	{
		return $this->getBookingModel()->createBooking($client);
	}
	public function deleteBooking($id): bool
	{
		return $this->getBookingModel()->deleteBooking($id);
	}
	public function getBookingByAttribute($attribute, $value): array
	{
		return $this->getBookingModel()->getBookingByAttribute($attribute, $value);
	}
}
