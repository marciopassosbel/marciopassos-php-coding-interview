<?php

namespace Src\models;

use Src\models\DogModel;
use Src\helpers\Helpers;

class BookingModel {

	private $bookingData;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings(): array
	{
		return $this->bookingData;
	}

	public function createBooking($data): array
	{
		$bookings = $this->getBookings();

		$data['id'] = end($bookings)['id'] + 1;

		$client = (new ClientModel())->getClientById($data['clientid']);
		// check discounts
		$discounts = new DiscountModel();
		$isClientEligibleForDiscount = $discounts->checkIfClientIsEligibleForDiscount($client);

		if ($isClientEligibleForDiscount) {
			$data['price'] = $discounts->applyDiscountsToPrice($client, $data['price']);
		}
		$bookings[] = $data;

		$this->helper->putJson($bookings, 'bookings');

		return $data;
	}

	public function deleteBooking($id): bool
	{
		$bookings = $this->getBookings();
		$deleted = false;
		foreach ($bookings as $key => $booking) {
			if ($booking['id'] == $id) {
				$deleted = true;
				unset($bookings[$key]);
			}
		}
		$this->helper->putJson($bookings, 'bookings');
		return $deleted;
	}

	public function getBookingByAttribute($attribute, $value): array
	{
		$bookings = $this->getBookings();
		// filter by attribute
		$bookings = array_filter($bookings, function($booking) use ($attribute, $value) {
			return $booking[$attribute] === $value;
		});
		return $bookings;
	}
}
