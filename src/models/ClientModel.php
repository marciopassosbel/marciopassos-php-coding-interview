<?php

namespace Src\models;

use Src\helpers\Helpers;

class ClientModel {

	private $clientData;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/clients.json');
		$this->clientData = json_decode($string, true);
	}

	public function getClients() {
		return $this->clientData;
	}

	public function createClient($data) {
		$clients = $this->getClients();

		$data['id'] = end($clients)['id'] + 1;
		$clients[] = $data;

		$this->helper->putJson($clients, 'clients');

		return $data;
	}

	public function updateClient($data): array
	{
		$updateClient = [];
		$clients = $this->getClients();
		foreach ($clients as $key => $client) {
			if ($client['id'] == $data['id']) {
				$clients[$key] = $updateClient = array_merge($client, $data);
			}
		}

		$this->helper->putJson($clients, 'clients');

		return $updateClient;
	}

	public function getClientById($id): array {
		$clients = $this->getClients();
		foreach ($clients as $client) {
			if ($client['id'] == $id) {
				return $client;
			}
		}
		return [];
	}

	public static function calculateAverageDogAge($clientId): float
	{
		$dogs = (new DogModel())->getDogs();
		// Filter dogs by clientId and calculate average age
		$clientDogs = array_filter($dogs, function($dog) use ($clientId) {
			return $dog['clientid'] == $clientId;
		});

		$totalAge = array_reduce($clientDogs, function($carry, $dog) {
			return $carry + $dog['age'];
		}, 0);

		return $totalAge / count($clientDogs);
	}

	/** Check if client has discount
	 *
	 * @param $data
	 * @return mixed
	 */
	public static function checkForDiscounts($data): array
	{
		$client = (new ClientModel)->getClientById($data['clientid']);

		$discount = new DiscountModel();

		$data['price'] = $discount->applyDiscountsToPrice($client, $data['price']);
		return $data;
	}
}
