<?php

namespace Src\models;

use Src\helpers\Helpers;

class DiscountModel {

	private $discountData;
	private $activeDiscounts;
	private $helper;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/discounts.json');
		$this->discountData = empty($this->discountData) ? json_decode($string, true) : $this->discountData;
		$this->activeDiscounts = $this->getActiveDiscounts();
	}

	public function getDiscounts(): array {
		return $this->discountData;
	}

	public function getActiveDiscounts(): array {
		return array_filter($this->discountData, function($discount) {
			return $discount['isActive'] === true;
		});
	}

	/**
	 * Applies discounts to the given price based on client eligibility.
	 */
	public function applyDiscountsToPrice($client, $originalPrice): float
	{
		$eligibleDiscounts = $this->getEligibleDiscountsForClient($client);
		$discountedPrice = $originalPrice;

		foreach ($eligibleDiscounts as $discount) {
			$discountedPrice -= $discount['discountPercentage'] / 100 * $originalPrice;
		}

		return $discountedPrice;
	}

	/**
	 * Checks if the client is eligible for any discount.
	 */
	public function checkIfClientIsEligibleForDiscount($client): bool
	{
		$eligibleDiscounts = $this->getEligibleDiscountsForClient($client);
		return !empty($eligibleDiscounts);
	}

	/**
	 * Identifies and returns the discounts a client is eligible for.
	 */
	private function getEligibleDiscountsForClient($client): array
	{
		$eligibleDiscounts = [];

		foreach ($this->activeDiscounts as $discount) {
			if ($discount['isActive']) {
				switch ($discount['conditionType']) {
					case 'dogAgeLessThan10':
						if (ClientModel::calculateAverageDogAge($client['id']) < 10) {
							$eligibleDiscounts[] = $discount;
						}
						break;
					// Add cases for other conditionTypes as needed
				}
			}
		}
		return $eligibleDiscounts;
	}
}
